#!/usr/bin/python3

###
# Copyright (c) 2002-2005, Jeremiah Fincher
# Copyright (c) 2011, James McCoy
# Copyright (c) 2021-2023, Stuart Prescott
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#   * Redistributions of source code must retain the above copyright notice,
#     this list of conditions, and the following disclaimer.
#   * Redistributions in binary form must reproduce the above copyright notice,
#     this list of conditions, and the following disclaimer in the
#     documentation and/or other materials provided with the distribution.
#   * Neither the name of the author of this software nor the name of
#     contributors to this software may be used to endorse or promote products
#     derived from this software without specific prior written consent.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
###

import atexit
import logging
import optparse
from pathlib import Path
import shutil
import sys
import time

import pytest


__version__ = "0.0.1"



class SupybotPytestPlugin:
    def pytest_sessionstart(self):
        self.started = time.time()

        # We need to do this before we import conf.
        self._create_config()
        self._create_world()
        self._configure()


    def _create_config(self):
        self.testconfdir = Path("test-conf")
        self.testconfdir.mkdir(exist_ok=True)
        self.registryFilename = self.testconfdir / 'test.conf'

        logging.warning("Creating test config for bot: %s", self.registryFilename)

        with open(self.registryFilename, 'w', encoding="UTF-8") as fd:
            fd.write("""\
supybot.directories.data: %(base_dir)s/test-data
supybot.directories.conf: %(base_dir)s/test-conf
supybot.directories.log: %(base_dir)s/test-logs
supybot.reply.whenNotCommand: True
supybot.log.stdout: False
supybot.log.stdout.level: ERROR
supybot.log.level: DEBUG
supybot.log.format: %%(levelname)s %%(message)s
supybot.log.plugins.individualLogfiles: False
supybot.protocols.irc.throttleTime: 0
supybot.reply.whenAddressedBy.chars: @
supybot.networks.test.server: should.not.need.this
supybot.networks.testnet1.server: should.not.need.this
supybot.networks.testnet2.server: should.not.need.this
supybot.networks.testnet3.server: should.not.need.this
supybot.nick: test
supybot.databases.users.allowUnregistration: True
""" % {'base_dir': Path.cwd()})

        for conf in ('users', 'channels', 'networks', 'ignores'):
            with open(self.testconfdir / f"{conf}.conf", "wb"):
                pass


    def _create_world(self):

        import supybot.registry as registry
        registry.open_registry(self.registryFilename)

        import supybot.log as log
        import supybot.conf as conf
        conf.allowEval = True
        conf.supybot.flush.setValue(False)

        import supybot.test as test
        # import supybot.plugin as plugin
        import supybot.utils as utils
        import supybot.world as world
        # import supybot.callbacks as callbacks
        world.startedAt = self.started

        self.conf = conf
        self.log = log
        # self.plugin = plugin
        self.test = test
        self.utils = utils
        self.world = world

    def _configure(self):
        self.log._logger.addFilter(_TestLogFilter())

        self.world.disableMultiprocessing = self.options.disableMultiprocessing

        if self.options.timeout:
            self.test.timeout = self.options.timeout

        if self.options.trace:
            traceFilename = self.conf.supybot.directories.log.dirize('trace.log')
            fd = open(traceFilename, 'w', encoding="UTF-8")
            sys.settrace(self.utils.gen.callTracer(fd))
            atexit.register(fd.close)
            atexit.register(lambda : sys.settrace(None))

        self.world.myVerbose = self.options.verbose

        if self.options.nonetwork:
            self.test.network = False
        if self.options.nosetuid:
            self.test.setuid = False

        self.log.testing = True
        self.world.testing = True

        self.conf.supybot.directories.plugins.setValue(self.options.pluginsDirs)

    def pytest_sessionfinish(self):
        logging.info("Cleaning up test config")
        if self.options.clean:
            shutil.rmtree(self.conf.supybot.directories.log())
            shutil.rmtree(self.conf.supybot.directories.conf())
            shutil.rmtree(self.conf.supybot.directories.data())


class _TestLogFilter(logging.Filter):
    bads = [
        'No callbacks in',
        'Invalid channel database',
        'Exact error',
        'Invalid user dictionary',
        'because of noFlush',
        'Queuing NICK',
        'Queuing USER',
        'IgnoresDB.reload failed',
        'Starting log for',
        'Irc object for test dying',
        'Last Irc,',
        ]
    def filter(self, record):
        for bad in self.bads:
            if bad in record.msg:
                return False
        return True


parser = optparse.OptionParser(usage='Usage: %prog [options] [plugins]',
                                version=f'Supybot {__version__}')
parser.add_option('-c', '--clean', action='store_true', default=False,
                    dest='clean', help='Cleans the various data/conf/logs'
                    'directories before running tests.')
parser.add_option('-t', '--timeout', action='store', type='float',
                    dest='timeout',
                    help='Sets the timeout, in seconds, for tests to return '
                    'responses.')
parser.add_option('-v', '--verbose', action='count', default=0,
                    help='Increase verbosity, logging extra information '
                        'about each test that runs.')
parser.add_option('', '--fail-fast', action='store_true', default=False,
                    help='Stop at first failed test.')
parser.add_option('', '--no-network', action='store_true', default=False,
                    dest='nonetwork', help='Causes the network-based tests '
                                            'not to run.')
parser.add_option('', '--no-setuid', action='store_true', default=False,
                    dest='nosetuid', help='Causes the tests based on a '
                                            'setuid executable not to run.')
parser.add_option('', '--trace', action='store_true', default=False,
                    help='Traces all calls made.  Unless you\'re really in '
                    'a pinch, you probably shouldn\'t do this; it results '
                    'in copious amounts of output.')
parser.add_option('', '--plugins-dir',
                    action='append', dest='pluginsDirs', default=[],
                    help='Looks in in the given directory for plugins '
                    'that were specified.')
parser.add_option('', '--disable-multiprocessing', action='store_true',
                    dest='disableMultiprocessing',
                    help='Disables multiprocessing stuff.')
(options, args) = parser.parse_args()


plugins = []
for arg in args:
    # these might be a plugin directory or might be a test.py
    pth = Path(arg)
    if not arg.endswith(".py"):
        logging.warning("Looking for implicit %s/test.py", arg)
        pth = pth / "test.py"

    if pth.name == "test.py":
        plugins.append(str(pth))
        needed_plugdir = ".." if pth.parent.name == "" else pth.parent.parent
        if needed_plugdir not in options.pluginsDirs:
            logging.warning("Adding %s to pluginsDirs", needed_plugdir)
            options.pluginsDirs.append(str(needed_plugdir))

if not plugins or not options.pluginsDirs:
    parser.print_help()
    sys.exit(-1)


pytest_options = ["-rsx"]
if options.verbose:
    pytest_options.append("-v")
if options.verbose >= 2:
    print("Showing locals")
    pytest_options.append("--showlocals")
if options.fail_fast:
    pytest_options.append("-x")

pytest_options.extend(plugins)


supybot_plugin_tester = SupybotPytestPlugin()
supybot_plugin_tester.options = options

result = pytest.main(pytest_options, plugins=[supybot_plugin_tester])

sys.exit(result)
